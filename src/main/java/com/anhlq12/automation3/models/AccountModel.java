package com.anhlq12.automation3.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.anhlq12.automation3.entities.Account;
import com.anhlq12.automation3.utils.DatabaseUtil;

public class AccountModel {

	private Connection conn;

	public AccountModel(Connection conn) {
		this.conn = conn;
	}

	public List<Account> getAll() {
		String query = "SELECT * FROM ACCOUNTS WHERE FLAG = 0";
		ResultSet rs = DatabaseUtil.fetch(conn, query);
		return getAccounts(rs);
	}

	public Account getByAccountID(int id) {
		String query = "SELECT * FROM ACCOUNT WHERE FLAG = 0 AND ACCOUNTID = " + id;
		ResultSet rs = DatabaseUtil.fetch(conn, query);
		List<Account> accounts = getAccounts(rs);
		return (accounts.size() > 0) ? accounts.get(0) : null;
	}

	public List<Account> getByKeywords(String kwd) {
		String query = "SELECT * FROM ACCOUNTS WHERE FLAG = 0 AND (FULLNAME LIKE '%" + kwd + "%' OR ACCOUNTID LIKE '%"
				+ kwd + "%')";
		System.out.println(query);
		ResultSet rs = DatabaseUtil.fetch(conn, query);
		return getAccounts(rs);
	}

	public void printOut(List<Account> accounts) {
		if (accounts.size() == 0) {
			System.out.println("Không có tài khoản nào");
			return;
		}
		for (Account account : accounts) {
			System.out.println(account);
		}
	}
	
	public List<String> getCSVFormat(List<Account> accounts) {
		List<String> ls = new ArrayList<String>();
		String s = "";
		for(Account account : accounts) {
			s = account.getId() + "," + account.getAccountId() + "," + account.getFullname() + "," + account.getAddress();
			ls.add(s);
		}
		return ls;
	}

	public List<Account> getAccounts(ResultSet rs) {
		List<Account> accounts = new ArrayList<Account>();
		try {
			while (rs.next()) {
				int id = rs.getInt("ID");
				int accountId  = rs.getInt("AccountID");
				String fullname = rs.getString("Fullname");
				String address = rs.getString("Address");
				boolean isActive = (rs.getInt("IsActive") == 1) ? true : false;
				accounts.add(new Account(id, accountId, fullname, address, isActive));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return accounts;
	}

	public boolean insertInto(Account account) {
		try {
			String query = "INSERT INTO ACCOUNTS (ACCOUNTID, FULLNAME, ADDRESS) VALUES (?, ?, ?)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, getMaxAccountID() + 1);
			stmt.setString(2, account.getFullname());
			stmt.setString(3, account.getAddress());
			return (stmt.executeUpdate() >= 1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean deleteFrom(int accountID) {
		String query = "UPDATE ACCOUNTS SET FLAG = 1 WHERE ACCOUNTID = (?)";
		try {
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, accountID);
			//stmt.execute
			return (stmt.executeUpdate() >= 1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public int getMaxAccountID() {
		String query = "SELECT TOP(1) * FROM ACCOUNTS ORDER BY ACCOUNTID DESC";
		ResultSet rs = DatabaseUtil.fetch(conn, query);
		try {
			rs.next();
			return rs.getInt("AccountID");
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}
}
