package com.anhlq12.automation3.entities;

public class Account {
	private int id;
	private int accountId;
	private String fullname;
	private String address;
	private boolean isActive;
	
	public Account(String fullname, String address) {
		this.fullname = fullname;
		this.address = address;
	}
	
	public Account(int id, int accountId, String fullname, String address) {
		this.id = id;
		this.accountId = accountId;
		this.fullname = fullname;
		this.address = address;
	}
	
	public Account(int id, int accountId, String fullname, String address, boolean isActive) {
		this.id = id;
		this.accountId = accountId;
		this.fullname = fullname;
		this.address = address;
		this.isActive = isActive;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", accountId=" + accountId + ", fullname=" + fullname + ", address=" + address
				+ ", isActive=" + isActive + "]";
	}
}
