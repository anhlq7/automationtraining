package com.anhlq12.automation3.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Utilities {

	private static final String DIR_PROJECT = System.getProperty("user.dir");
	private static final String DIR_CONFIG = DIR_PROJECT + "\\" + "config.properties";

	public static String getProperty(String key) {
		Properties p = new Properties();
		FileInputStream fis;
		try {
			fis = new FileInputStream(DIR_CONFIG);
			p.load(fis);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return p.getProperty(key);
	}

	public static boolean writeCSV(String filename, String[] headers, List<String> lines) {
		String pathname = DIR_PROJECT + "\\" + filename;
		try {
			Writer w = new FileWriter(new File(pathname));
			StringBuilder sb = new StringBuilder();
			String header = "";
			for (int i = 0; i < headers.length - 1; i++) {
				header += headers[i] + ",";
			}
			header += headers[headers.length - 1];
			sb.append(header);
			for (String l : lines) {
				sb.append("\n");
				sb.append(l);
			}
			w.append(sb.toString());
			w.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public static List<String[]> readCSV(String filename) {
		List<String[]> ls = new ArrayList<String[]>();
		String pathname = DIR_PROJECT + "\\" + filename;
		BufferedReader r;
		String row = "";
		try {
			r = new BufferedReader(new FileReader(pathname));
			row = r.readLine(); //ignore header
			while ((row = r.readLine()) != null) {
				String[] data = row.split(",");
				ls.add(data);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			ls = new ArrayList<String[]>();
		}
		return ls;
	}

}
